class arbol{
	constructor(valor, nivel){
		this.Raiz=null;

    
	}
    
    INR(valor, nivel,){
      var nuevoN=new nodo(valor, nivel);

      if (this.Raiz===null) {
       this.Raiz=nuevoN;
      }else{
       this.INH(this.Raiz, nuevoN, nivel);
      }
    }


    INH(nodo, nuevoN, nivel){
     if(nuevoN.valor<nodo.valor){ 
     if (nodo.Izquierda===null)	nodo.Izquierda=nuevoN;
     else this.INH(nodo.Izquierda, nuevoN, nivel);
    }else{
    	if (nodo.Derecha===null) 
    		nodo.Derecha=nuevoN;
    	 else{
    		this.INH(nodo.Derecha, nuevoN, nivel);
    	  }
       }
    }

    buscarnodo(nodo, enn){
      if (nodo==null)return null;
      
      else if(enn<nodo.valor){
        return this.buscarnodo(nodo.Izquierda, enn);
      }else if(enn>nodo.valor){
        return this.buscarnodo(nodo.Derecha, enn);
      }else{
        return nodo;
      }
    }

    camino(nodo, enn, reco){
      if (this.buscarnodo(nodo, enn)==null)return null;
      var rec=reco;
      if (enn===21)return 21;
      
      else if(enn<nodo){
        reco=reco+nodo.valor+',';
        return this.camino(nodo.Izquierda, enn, reco);
      }else if(enn>nodo.valor){
        reco=reco+nodo.valor+',';
        return this.camino(nodo.Derecha, enn, reco);
      }else{
        return rec+enn;
      }
    }

su(nodo, rn, suma){
  if (this.buscarnodo(nodo, rn)==null)return null;
      var sum=suma;

      if (rn===21)return 21;
      
      else if(rn<nodo){
        sum=sum+nodo.valor;
        return this.su(nodo.Izquierda, rn, sum);
      }else if(rn>nodo.valor){
        sum=sum+nodo.valor;
        return this.su(nodo.Derecha, rn, sum);
      }else{
        return sum+rn;
      }
    }
}